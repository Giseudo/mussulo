<?php $_pagename = 'inclusive'; include "header.php"; ?>

	<section class="all-inclusive inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>All Inclusive</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
				</div>
			</div>
		</div>

		<div class="gastronomia tabs">
			<h1>Diversidade Gastronomica</h1>
			<article class="-active">
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/beach-club-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/beach-club.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Restaurante Rio Zare</h2>
						<div class="post">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

<?php get_footer() ?>
