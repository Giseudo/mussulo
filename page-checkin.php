<?php $_pagename = 'checkin'; include "header.php"; ?>

	<section class="checkin inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Checkin</h1>
		</div>
		<div class="info col-xs-12 col-sm-8 col-md-9">
			<h2>Faça seu web check-in e ganhe tempo</h2>
			<p>As informações que seguem abaixo correspondem a Ficha Nacional de Registros de Hóspedes.
É exatamentea mesma ficha que você está acostumado a preencher nos guichês de check-in nos hotéis brasileiros.</p>
		</div>
		<form action="">
			<ul>
			</ul>
			<fieldset class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<ul>
					<li class="col-xs-12">
						<input type="submit" name="send" value="Enviar" class="button-default">
						<button class="more-people button-default">Adicionar<br /> Acompanhante</button>
					</li>
				</ul>
			</fieldset>
		</form>

		<script type="text/template" id="checkin-template">
			<fieldset class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<h2>Dados Pessoais</h2>
				<ul class="row">
					<li class="col-xs-12 col-sm-6">
						<label for="contato_nome">Nome:</label>
						<input type="text" id="contato_nome" name="nome[]">
					</li>
					<li class="col-xs-12 col-sm-6">
						<label for="contato_email">Email:</label>
						<input type="email" id="contato_email" name="email[]">
					</li>
					<li class="col-xs-12 col-sm-6">
						<label for="contato_telefone">Telefone:</label>
						<input type="text" id="contato_telefone" name="telefone[]">
					</li>
					<li class="col-xs-12 col-sm-3">
						<label for="contato_nascimento">Nascimento:</label>
						<input type="text" id="contato_nascimento" name="nascimento[]">
					</li>
					<li class="col-xs-12 col-sm-3">
						<label for="contato_sexo">Sexo:</label>
						<select name="sexo[]" id="contato_sexo">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-5">
						<label for="contato_nacionalidade">Nacionalidade:</label>
						<input type="text" id="contato_nacionalidade" name="nacionalidade[]">
					</li>
					<li class="col-xs-12 col-sm-5">
						<label for="contato_pais">País:</label>
						<select name="pais[]" id="contato_pais">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-2">
						<label for="contato_estado">Estado:</label>
						<select name="estado[]" id="contato_estado">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-4">
						<label for="contato_cidade">Cidade:</label>
						<select name="cidade[]" id="contato_cidade">
							<option value=""></option>
							<option value="M">Masculino</option>
							<option value="F">Feminino</option>
							<option value="O">Outro</option>
						</select>
					</li>
					<li class="col-xs-12 col-sm-6">
						<label for="contato_endereco">Endereço:</label>
						<input type="text" id="contato_endereco" name="endereco[]">
					</li>
					<li class="col-xs-12 col-sm-2">
						<label for="contato_numero">Numero:</label>
						<input type="number" id="contato_numero" name="numero[]">
					</li>
					<li class="col-xs-12">
						<label for="contato_complemento">Complemento:</label>
						<input type="text" id="contato_complemento" name="complemento[]">
					</li>
				</ul>
			</fieldset>

			<fieldset class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<h2>Documento</h2>
				<ul class="row">
					<li class="col-xs-12 col-sm-6 checkbox">
						<label for="contato_documento_br"><input type="radio" id="contato_documento_br" class="tab-radio" data-target="#brasileiro" name="documento[]" value="Brasileiro" checked> Sou brasileiro</label>
					</li>
					<li class="col-xs-12 col-sm-6 checkbox">
						<label for="contato_documento_en"><input type="radio" id="contato_documento_en" class="tab-radio" data-target="#estrangeiro" name="documento[]" value="Estrangeiro"> Sou estrangeiro</label>
					</li>
				</ul>
				<div class="tabs">
					<ul id="brasileiro" class="tab -active col-xs-12 col-sm-4">
						<li>
							<label for="contato_cpf">CPF</label>
							<input type="text" name="cpf[]" id="contato_cpf">
						</li>
						<li>
							<label for="contato_cpf">RG</label>
							<input type="text" name="rg[]" id="contato_rg">
						</li>
					</ul>
					<ul id="estrangeiro" class="tab col-xs-12 col-sm-6">
						<li>
							<label for="contato_passaporte">Passaporte</label>
							<input type="text" name="passaporte[]" id="contato_passaporte">
						</li>
					</ul>
				</div>
			</fieldset>
		</script>
	</section>

<?php get_footer() ?>
