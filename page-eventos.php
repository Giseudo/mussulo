<?php $_pagename = 'eventos'; include "header.php"; ?>

	<section class="eventos inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>Grupos<br /> e Eventos</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<p>O Centro de Convenções Angola do Mussulo Resort by Mantra oferece uma ampla infraestrutura com o atencioso atendimento de uma equipe especializada e, a mais alta tecnologia e serviços de comunicação para eventos, congressos e reuniões de negócios.</p>
					<p>Conta com duas salas preparadas para brindar um espaço confortável, seguro e flexível para as solicitações pontuais de qualquer tipo de evento.
As salas são reversíveis, podendo ser um ambiente único com mais capacidade, ideal para um evento de maior porte.</p>
				</div>
				<div class="gallery -desktop">
					<div class="swiper-container">
						<ul class="swiper-wrapper">
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
						</ul>
					</div>
					<button class="prev"></button>
					<button class="next"></button>
				</div>
			</div>
		</div>

		<div class="salas">
			<article class="col-xs-12 col-sm-4">
				<h1>Sala A<br /> Rio Cuanza</h1>
				<ul>
					<li>Internet Wi Fi</li>
					<li>Pontos de energia com entradas bi e trifásicas</li>
					<li>Refrigeração com ar condicionados split</li>
					<li>Comprimento: 15,8m</li>
					<li>Altura: 4,5m</li>
					<li>Área: 186,4m</li>
					<li class="head">Formatos e capacidade</li>
					<li>Auditório: 300 pessoas</li>
					<li>Escolar com cadeiras com braço: 160 pessoas</li>
					<li>Escolar com pranchas: 105 pessoas</li>
					<li>Espinha de peixe sem pranchas: 240 pessoas</li>
					<li>Espinha de peixe com pranchas: 84 pessoas</li>
					<li>Ilhas circulares sem mesas: 180 pessoas</li>
					<li>Ilhas com mesas retangulares: 144 pessoas</li>
					<li>Ilhas com mesas redondas: 96 pessoas</li>
					<li>“U” com pranchas: 57 pessoas</li>
					<li>“U” sem pranchas: 74 pessoas</li>
					<li>Formatos e capacidades montados com amplo espaço de circulação nas laterais, na parte traseira e dianteira da sala.</li>
				</ul>
			</article>
			<article class="col-xs-12 col-sm-4">
				<h1>Sala A<br /> Rio Cuanza</h1>
				<ul>
					<li>Internet Wi Fi</li>
					<li>Pontos de energia com entradas bi e trifásicas</li>
					<li>Refrigeração com ar condicionados split</li>
					<li>Comprimento: 15,8m</li>
					<li>Altura: 4,5m</li>
					<li>Área: 186,4m</li>
					<li class="head">Formatos e capacidade</li>
					<li>Auditório: 300 pessoas</li>
					<li>Escolar com cadeiras com braço: 160 pessoas</li>
					<li>Escolar com pranchas: 105 pessoas</li>
					<li>Espinha de peixe sem pranchas: 240 pessoas</li>
					<li>“U” sem pranchas: 74 pessoas</li>
					<li>Formatos e capacidades montados com amplo espaço de circulação nas laterais, na parte traseira e dianteira da sala.</li>
				</ul>
			</article>
			<article class="col-xs-12 col-sm-4">
				<h1>Sala A<br /> Rio Cuanza</h1>
				<ul>
					<li>Auditório: 300 pessoas</li>
					<li>Escolar com cadeiras com braço: 160 pessoas</li>
					<li>Escolar com pranchas: 105 pessoas</li>
					<li>Espinha de peixe sem pranchas: 240 pessoas</li>
					<li>Espinha de peixe com pranchas: 84 pessoas</li>
					<li>Ilhas circulares sem mesas: 180 pessoas</li>
					<li>Ilhas com mesas retangulares: 144 pessoas</li>
					<li>Ilhas com mesas redondas: 96 pessoas</li>
					<li>“U” com pranchas: 57 pessoas</li>
					<li>“U” sem pranchas: 74 pessoas</li>
					<li>Formatos e capacidades montados com amplo espaço de circulação nas laterais, na parte traseira e dianteira da sala.</li>
				</ul>
			</article>
			<footer>
				<h1>Para maiores informações contate nossos assessores</h1>
				<a href="mailto:eventos@mussulobymantra.com.br">eventos@mussulobymantra.com.br</a>
				<span class="phone">+55 (83) 3298-2750</span>
			</footer>
		</div>
	</section>

<?php get_footer() ?>
