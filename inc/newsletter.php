<form action="" class="newsletter">
	<div class="container">
		<div class="col-xs-12 col-sm-4 col-sm-offset-1">
			<h1>Cadastre-se em nossa <span>Newsletter</span></h1>
		</div>
		<fieldset class="col-xs-12 col-sm-5">
			<div class="holder">
				<input type="email" name="email" required="required" placeholder="DIGITE SEU EMAIL">
				<button class="glyphicon glyphicon-envelope"></button>
			</div>
		</fieldset>
	</div>
</form>
