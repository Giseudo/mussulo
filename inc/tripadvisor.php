<section class="tripadvisor">
	<div class="container">
		<div class="col-xs-12 col-sm-3 col-sm-offset-2">
			<h1 class="logo"><img src="<?php bloginfo('template_url') ?>/assets/img/tripadvisor.jpg" alt="Tripadvisor"></h1>
		</div>
		<div class="review col-xs-12 col-sm-5">
			<blockquote>
				<p>“Resort maravilhoso. Não perca tempo”</p>
				<footer>
					<cite><a href="#">Robson Porto, PB</a></cite>
				</footer>
			</blockquote>
			<a href="" class="button-default">Ver avaliações</a>
		</div>
	</div>
</section>
