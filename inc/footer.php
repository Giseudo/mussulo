<footer class="footer">
	<h1 class="partners"><img src="<?php bloginfo('template_url') ?>/assets/img/mussulo-mantra.png" alt="Mussulo Resort, Mantra Group"></h1>
	<p>Litoral Sul da Paraíba, Município de Conde, Brasil</p>
	<nav class="social">
		<?php get_template_part('inc/nav', 'social'); ?>
	</nav>
</footer>
