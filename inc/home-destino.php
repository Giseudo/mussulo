<section class="destination">
	<header class="container">
		<h1>O seu destino</h1>
		<h2>Na Paraíba</h2>
	</header>
	<article class="col-xs-12 col-sm-4">
		<a href="#" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/destino-01.jpg);">
			<span class="brackets"></span>
			<img src="<?php bloginfo('template_url') ?>/assets/img/delete/all-inclusive.png" alt="All Inclusive">
		</a>
	</article>
	<article class="col-xs-12 col-sm-4">
		<a href="#" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/destino-02.jpg);">
			<span class="brackets"></span>
			<img src="<?php bloginfo('template_url') ?>/assets/img/delete/o-resort.png" alt="O Resort">
		</a>
	</article>
	<article class="col-xs-12 col-sm-4">
		<a href="#" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/destino-03.jpg);">
			<span class="brackets"></span>
			<img src="<?php bloginfo('template_url') ?>/assets/img/delete/acomodacoes.png" alt="Acomodações">
		</a>
	</article>
</section>
