<?php $_pagename = 'home'; include "header.php"; ?>

	<?php get_template_part('inc/featured'); ?>

	<section class="home">
		<?php get_template_part('inc/home', 'destino'); ?>
		<?php get_template_part('inc/home', 'paraibadisiaco'); ?>
	</section>

<?php get_footer() ?>
