<?php $_pagename = 'contato'; include "header.php"; ?>

	<section class="contato inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Contato</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="address">
				<h2>Mussulo Resort by Mantra</h2>
				<p>Loteamento Cidade Balneário Novo Mundo,<br /> s/n Lote 01 Quadra Z33, Conde Paraíba, Brasil</p>
				<p><strong>Tel.:</strong> +55 83 32982750<br /> <strong>Fax:</strong> +55 83 32982752</p>
				<ul class="emails row">
					<li class="col-xs-12 col-sm-6">
						<h3>Reservas individuais</h3>
						<span>reservas@mussulobymantra.com.br</span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>RH</h3>
						<span>rh@mussulobymantra.com.br</span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Reservas Grupos e Eventos</h3>
						<span>eventos@mussulobymantra.com.br</span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Marketing</h3>
						<span>marketing@mussulobymantra.com.br</span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Central de Atendimento</h3>
						<span>central.atendimento@mussulobymantra.com.br</span>
					</li>
					<li class="col-xs-12 col-sm-6">
						<h3>Gerencia geral</h3>
						<span>gerencia.geral@mussulobymantra.com.br </span>
					</li>
				</ul>
			</div>
		</div>
		<form action="">
			<h1>Fale Conosco</h1>
			<ul class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-offset-3">
				<li class="col-xs-12 col-sm-6">
					<label for="contato_nome">Nome:</label>
					<input type="text" id="contato_nome" name="nome">
				</li>
				<li class="col-xs-12 col-sm-6">
					<label for="contato_email">Email:</label>
					<input type="email" id="contato_email" name="email">
				</li>
				<li class="col-xs-12 col-sm-6">
					<label for="contato_telefone">Telefone:</label>
					<input type="text" id="contato_telefone" name="telefone">
				</li>
				<li class="col-xs-12 col-sm-6">
					<label for="contato_assunto">Assunto:</label>
					<input type="text" id="contato_assunto" name="assunto">
				</li>
				<li class="col-xs-12">
					<label for="contato_mensagem">Mensagem:</label>
					<textarea name="mensagem" id="contato_mensagem"></textarea>
				</li>
				<li class="col-xs-12">
					<input type="submit" name="send" value="Enviar" class="button-default">
				</li>
			</ul>
		</form>
	</section>

<?php get_footer() ?>
