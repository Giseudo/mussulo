/*global $, Mushi, console */

Mushi.resort = {
	init: function () {
		console.log('Resort started');
		this.tabsInit();
	},

	tabsInit : function () {
		$('.tabs article > header').bind('click', function () {
			var newHeight = $('.content', this.parentNode).innerHeight();

			$('.tabs .container').stop(false, false).animate({ height : 0 });

			$('.tabs article').removeClass('-active');
			$(this).parent().addClass('-active');

			$('article.-active .container').stop(false, false).animate({ height : newHeight }, function () {
				$('body, html').animate({ scrollTop : $('article.-active').position().top - $('header.top').height() - $('form.reserva.-top').height() });
			});
		});
	}
};
