<?php $_pagename = 'localizacao'; include "header.php"; ?>

	<section class="localizacao inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Localização</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="post">
				<p>Localizado 20 km da capital, João Pessoa, na região turística da Costa do Conde, o Mussulo by Mantra trouxe para a região uma nova experiência em hospedagem e a oportunidade de desfrutar de conforto , exclusividade e personalizado serviço de primeira classe, e  a incrível beleza dos pontos turísticos naturais que concentram atrativos únicos,  como florestas virgens , arrecifes, praias e piscinas naturais.</p>
			</div>
		</div>
		<div class="map col-xs-12">
			<h1 class="subtitle">Como chegar</h1>
			<nav>
				<button class="-active" data-target="pb">Aeroporto<br /> de João Pessoa</button>
				<button data-target="pe">Aeroporto<br /> de Recife</button>
				<button data-target="rn">Aeroporto<br /> de Natal</button>
			</nav>
			<div id="google-map"></div>
		</div>
		<div class="paraibadisiaco col-xs-12">
			<div class="container">
				<h1 class="subtitle">Paraibadisíaco</h1>
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<div class="swiper-slide col-xs-12 col-sm-4">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/paraibadisiaco-01.jpg" alt="Paraibadisiaco">
						</div>
						<div class="swiper-slide col-xs-12 col-sm-4">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/paraibadisiaco-02.jpg" alt="Paraibadisiaco">
						</div>
						<div class="swiper-slide col-xs-12 col-sm-4">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/paraibadisiaco-03.jpg" alt="Paraibadisiaco">
						</div>
					</div>
					<div class="swiper-pagination"></div>
				</div>
				<p>Bem-vindo à Paraíba ! O ponto mais oriental da América. Este estado , localizado na região nordeste do Brasil, traz beleza , tradição , natureza e uma grande diversidade de expressões culturais. Cantada em verso e prosa por artistas de todo o país , a Paraíba é uma experiência única que reúne turismo , folclore e diversão como ninguém. Paraíba é um destino onde os visitantes podem combinar a paz de uma vida familiar tranquila e desfrutar de paisagens perfeitas do litoral e florestas naturais exuberantes , shoppings, o melhor da cozinha , entretenimento e diversão.</p>
			</div>
		</div>
	</section>

<?php get_footer() ?>
