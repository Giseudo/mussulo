<?php $_pagename = 'pacotes'; include "header.php"; ?>

	<section class="pacotes inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="title col-xs-12 col-sm-4 col-md-3">
			<h1>Pacotes & Promoções</h1>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="post">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

		<div class="main">
			<aside class="col-xs-12 col-sm-3 col-md-2">
				<ul>
					<li class="-active">
						<a href="#">Pacotes</a>
						<ul class="submenu">
							<li>
								<a href="#">Noite de Nupcias</a>
							</li>
							<li>
								<a href="#">Roteiro Paraibadisíaco</a>
							</li>
							<li>
								<a href="#">Natal, Reveillon, Férias de Verão e Carnaval</a>
							</li>
							<li>
								<a href="#">Feriado da Independência</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Promoções</a>
						<ul class="submenu">
							<li>
								<a href="#">Noite de Nupcias</a>
							</li>
							<li>
								<a href="#">Roteiro Paraibadisíaco</a>
							</li>
							<li>
								<a href="#">Natal, Reveillon, Férias de Verão e Carnaval</a>
							</li>
							<li>
								<a href="#">Feriado da Independência</a>
							</li>
						</ul>
					</li>
				</ul>
			</aside>

			<div class="-list col-xs-12 col-sm-9 col-md-10">
				<div class="row">
					<article class="col-xs-12 col-sm-6">
						<div class="thumb">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
							<div class="holder">
								<a href="<?php bloginfo('url') ?>/hello-world" class="button-default">Ver Pacote</a>
							</div>
							<a href="<?php bloginfo('url') ?>/hello-world" class="overlay"></a>
						</div>
						<a href="<?php bloginfo('url') ?>/hello-world" class="permalink">Noite de Nupcias</a>
					</article>
					<article class="col-xs-12 col-sm-6">
						<div class="thumb">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
							<div class="holder">
								<a href="<?php bloginfo('url') ?>/hello-world" class="button-default">Ver Pacote</a>
							</div>
							<a href="<?php bloginfo('url') ?>/hello-world" class="overlay"></a>
						</div>
						<a href="<?php bloginfo('url') ?>/hello-world" class="permalink">Noite de Nupcias</a>
					</article>
					<article class="col-xs-12 col-sm-6">
						<div class="thumb">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
							<div class="holder">
								<a href="<?php bloginfo('url') ?>/hello-world" class="button-default">Ver Pacote</a>
							</div>
							<a href="<?php bloginfo('url') ?>/hello-world" class="overlay"></a>
						</div>
						<a href="<?php bloginfo('url') ?>/hello-world" class="permalink">Noite de Nupcias</a>
					</article>
					<article class="col-xs-12 col-sm-6">
						<div class="thumb">
							<img src="<?php bloginfo('template_url') ?>/assets/img/delete/pacote-01.jpg" alt="Pacote 01">
							<div class="holder">
								<a href="<?php bloginfo('url') ?>/hello-world" class="button-default">Ver Pacote</a>
							</div>
							<a href="<?php bloginfo('url') ?>/hello-world" class="overlay"></a>
						</div>
						<a href="<?php bloginfo('url') ?>/hello-world" class="permalink">Noite de Nupcias</a>
					</article>
				</div>
			</div>
		</div>
	</section>

<?php get_footer() ?>
