<?php $_pagename = 'resort'; include "header.php"; ?>

	<section class="resort inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>O Resort</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<p>Mussulo Resort by Mantra , o primeiro Resort ALL INCLUSIVE da região, combina uma localização privilegiada próxima ao oceano com as riquezas culturais e paisagísticas do Estado da Paraíba, um verdadeiro paraíso a apenas 20 km da cidade de João Pessoa, capital do estado.</p>
					<p>Este lugar Paraibadisíaco em pleno polo turístico da Costa do Conde, brinda a uma nova experiência em hospedagem e à oportunidade de desfrutar com conforto, exclusividade e um serviço personalizado de primeiro nível, as belezas naturais e os inacreditáveis pontos turísticos paraibanos que concentram atrativos únicos como bosques virgens, arrecifes, praias e piscinas naturais altamente valorizados pelos turistas do Brasil e de todo o mundo.</p>
				</div>
				<ul class="espec">
					<li class="area col-xs-12 col-sm-4">
						<span>96</span>
						<span>mil m²</span>
					</li>
					<li class="bangalo col-xs-12 col-sm-4">
						<span>102</span>
						<span>Magnificos Bangalôs</span>
					</li>
					<li class="estrutura col-xs-12 col-sm-4">
						<span>Estrutura pensada<br /> para o completo<br /> lazer e descanso.</span>
					</li>
				</ul>
				<div class="gallery -desktop">
					<div class="swiper-container">
						<ul class="swiper-wrapper">
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
							<li class="swiper-slide">
								<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
							</li>
						</ul>
					</div>
					<button class="prev"></button>
					<button class="next"></button>
				</div>

			</div>
		</div>

		<div class="areas tabs">
			<h1>Conheça nossas áreas</h1>
			<article class="-active">
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/beach-club-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/beach-club.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Beach club</h2>
						<div class="post">
							<p>Sobre a belíssima praia de Tabatinga, próxima ao Mussulo Resort by Mantra, está localizado o exclusivo Mussulo Beach Club, o complexo de praia mais completo da Paraíba.</p>
							<p>Um lugar perfeito para relaxar e desfrutar desta paradisíaca praia com uma vista natural única com todos os serviços exclusivos de bar.</p>
							<p>Este beach club à beira mar e à apenas 2km do resort é ideal para casamentos e aniversários, assim como eventos empresariais que buscam o equilíbrio perfeito entre um ambiente descontraído de uma rica beleza natural, equipamentos modernos e um serviço gastronômico de primeiro nível.</p>
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
			<article>
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/kids-club-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/kids-club.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Beach club</h2>
						<div class="post">
							<p>Sobre a belíssima praia de Tabatinga, próxima ao Mussulo Resort by Mantra, está localizado o exclusivo Mussulo Beach Club, o complexo de praia mais completo da Paraíba.</p>
							<p>Um lugar perfeito para relaxar e desfrutar desta paradisíaca praia com uma vista natural única com todos os serviços exclusivos de bar.</p>
							<p>Este beach club à beira mar e à apenas 2km do resort é ideal para casamentos e aniversários, assim como eventos empresariais que buscam o equilíbrio perfeito entre um ambiente descontraído de uma rica beleza natural, equipamentos modernos e um serviço gastronômico de primeiro nível.</p>
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
			<article>
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/health-club-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/health-club.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Beach club</h2>
						<div class="post">
							<p>Sobre a belíssima praia de Tabatinga, próxima ao Mussulo Resort by Mantra, está localizado o exclusivo Mussulo Beach Club, o complexo de praia mais completo da Paraíba.</p>
							<p>Um lugar perfeito para relaxar e desfrutar desta paradisíaca praia com uma vista natural única com todos os serviços exclusivos de bar.</p>
							<p>Este beach club à beira mar e à apenas 2km do resort é ideal para casamentos e aniversários, assim como eventos empresariais que buscam o equilíbrio perfeito entre um ambiente descontraído de uma rica beleza natural, equipamentos modernos e um serviço gastronômico de primeiro nível.</p>
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
			<article>
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Beach club</h2>
						<div class="post">
							<p>Sobre a belíssima praia de Tabatinga, próxima ao Mussulo Resort by Mantra, está localizado o exclusivo Mussulo Beach Club, o complexo de praia mais completo da Paraíba.</p>
							<p>Um lugar perfeito para relaxar e desfrutar desta paradisíaca praia com uma vista natural única com todos os serviços exclusivos de bar.</p>
							<p>Este beach club à beira mar e à apenas 2km do resort é ideal para casamentos e aniversários, assim como eventos empresariais que buscam o equilíbrio perfeito entre um ambiente descontraído de uma rica beleza natural, equipamentos modernos e um serviço gastronômico de primeiro nível.</p>
						</div>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

<?php get_footer() ?>
