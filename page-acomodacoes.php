<?php $_pagename = 'resort'; include "header.php"; ?>

	<section class="resort acomodacoes inner">
		<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/resort-top.jpg)"	></header>
		<div class="container">
			<div class="title col-xs-12 col-sm-4 col-md-3">
				<h1>Acomodações</h1>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="post">
					<p>O Mussulo Resort by Mantra conta com 102 confortáveis bangalôs bem decorados sob a influência eco-étnica das culturas paraibana e angolana presentes em toda a região. </p>
					<p>Com uma estrutura suave e tropical com muita madeira, sisal, fibras de coco e peças do rico artesanato afro-nordestino, cada bangalô se destaca por sua elegância, equipamentos, detalhes exclusivos e paradisíacas vistas. Tudo para fazer de sua estada uma experiência inesquecível e de puro prazer.</p>
					<h4>Todos os bangalôs possuem:</h4>
					<ul>
						<li>TV a cabo (na sala e no quarto de casal)</li>
						<li>Linha telefônica</li>
						<li>Minibar</li>
						<li>Cofre</li>
						<li>Internet Wi Fi</li>
						<li>Secador de cabelo</li>
						<li>Amenities exclusivos para banho</li>
						<li>Ar condicionado</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="areas tabs">
			<h1>Veja nossos Bangalôs</h1>
			<article class="-active">
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/beach-club-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/beach-club.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Deluxe</h2>
						<div class="post">
						</div>
						<ul class="espec">
							<li class="area col-xs-12 col-sm-4">
								<span>80</span>
								<span>m²</span>
							</li>
							<li class="capacidade col-xs-12 col-sm-4">
								<span>Capacidade</span>
								<span>02</span>
								<span>Hospedes</span>
							</li>
							<li class="estrutura col-xs-12 col-sm-4">
								<span>01</span>
								<span>Suíte com cama de casal,<br /> sala de estar com ventilador<br /> de teto e cozinha americana</span>
							</li>
						</ul>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>

			<article>
				<header style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/kids-club-bg.jpg)">
					<img src="<?php bloginfo('template_url') ?>/assets/img/delete/kids-club.png" alt="Beach Club">
					<span class="brackets -top"></span>
					<span class="brackets -bottom"></span>
				</header>
				<div class="container">
					<div class="content col-xs-12 col-md-8 col-md-offset-2">
						<h2>Deluxe</h2>
						<div class="post">
						</div>
						<ul class="espec">
							<li class="area col-xs-12 col-sm-4">
								<span>80</span>
								<span>m²</span>
							</li>
							<li class="capacidade col-xs-12 col-sm-4">
								<span>Capacidade</span>
								<span>02</span>
								<span>Hospedes</span>
							</li>
							<li class="estrutura col-xs-12 col-sm-4">
								<span>01</span>
								<span>Suíte com cama de casal,<br /> sala de estar com ventilador<br /> de teto e cozinha americana</span>
							</li>
						</ul>
						<div class="gallery -desktop">
							<div class="swiper-container">
								<ul class="swiper-wrapper">
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
									<li class="swiper-slide">
										<a href="<?php bloginfo('template_url') ?>/assets/img/delete/centro-de-convencoes-bg.jpg"><img src="<?php bloginfo('template_url') ?>/assets/img/delete/gallery-thumb-01.jpg" alt="Gallery"></a>
									</li>
								</ul>
							</div>
							<button class="prev"></button>
							<button class="next"></button>
						</div>
					</div>
				</div>
			</article>

		</div>
	</section>

<?php get_footer() ?>
